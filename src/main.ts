import { MicroserviceOptions } from "@nestjs/microservices";
import { NestFactory } from "@nestjs/core";
import { options } from "./client.options";
import { AppModule } from "./app.module";

const bootstrap = async () => {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      ...options,
      logger: console,
    },
  );
  app.listen(() => {
    console.info("🚀 microservice payment ready 🚀");
  });
};

bootstrap().catch(console.error);
