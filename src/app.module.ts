import { PaymentModule } from "./services/payment/payment.module";
import { HealthModule } from "./services/health/health.module";
import { SequelizeModule } from "@nestjs/sequelize";
import { Module } from "@nestjs/common";

const configDatabase = require("./database/config");

@Module({
  imports: [
    SequelizeModule.forRoot({
      ...configDatabase,
      autoLoadModels: true,
      synchronize: false,
    }),
    PaymentModule,
    HealthModule,
  ],
})
export class AppModule {}
