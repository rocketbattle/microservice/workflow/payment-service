import { ClientOptions, Transport } from "@nestjs/microservices";
import { join } from "path";

export const options: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    package: ["payment.v1", "grpc.health.v1"],
    url: "0.0.0.0:5000",
    protoPath: [
      join(__dirname, "./services/payment/payment.proto"),
      join(__dirname, "./services/health/health.proto"),
    ],
  },
};
