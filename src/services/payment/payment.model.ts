import { Column, Model, Table } from "sequelize-typescript";

@Table({ tableName: "payments", modelName: "payment" })
export class Payment extends Model<Payment> {
  @Column({
    autoIncrement: true,
    primaryKey: true,
  })
  id?: number;

  @Column
  value?: number;

  @Column({ field: "transaction_id", defaultValue: "PENDING" })
  transactionId?: string;

  @Column({ field: "phase_id" })
  phaseId?: string;

  @Column({ field: "created_at" })
  createdAt?: Date;
}
