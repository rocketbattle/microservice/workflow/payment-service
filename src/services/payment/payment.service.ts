import { PaymentById } from "./payment";
import { Payment } from "./payment.model";
import { Observable } from "rxjs";

export interface PaymentService {
  findOne(data: PaymentById): Observable<Payment>;
}
