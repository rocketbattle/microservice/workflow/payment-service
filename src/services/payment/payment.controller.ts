import { GrpcMethod } from "@nestjs/microservices";
import { InjectModel } from "@nestjs/sequelize";
import { Controller } from "@nestjs/common";
import { Payment } from "./payment.model";
import { PaymentById } from "./payment";
import { from, Observable } from "rxjs";

@Controller()
export class PaymentController {
  constructor(@InjectModel(Payment) private readonly model: typeof Payment) {}

  @GrpcMethod("PaymentService", "FindOne")
  findOne(data: PaymentById): Observable<Payment> {
    return from(
      this.model.findOne({
        where: {
          id: data.id,
        },
      }),
    );
  }
}
