import { PaymentController } from "./payment.controller";
import { ClientsModule } from "@nestjs/microservices";
import { SequelizeModule } from "@nestjs/sequelize";
import { options } from "../../client.options";
import { Payment } from "./payment.model";
import { Module } from "@nestjs/common";

@Module({
  imports: [
    ClientsModule.register([
      {
        name: "PAYMENT_PACKAGE",
        ...options,
      },
    ]),
    SequelizeModule.forFeature([Payment]),
  ],
  controllers: [PaymentController],
})
export class PaymentModule {}
