export interface PaymentById {
  id: number;
}

export interface PaymentByTransactionId {
  transactionId: string;
}
