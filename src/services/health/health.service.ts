import { HealthCheckRequest, HealthCheckResponse } from "./health";
import { Observable } from "rxjs";

/**
 * The interface of the default GRPC HealthService,
 * according to the GRPC specs
 */
export interface HealthService {
  /**
   * Checks if the given service is up using the standart health check
   * specification of GRPC.
   *
   * https://github.com/grpc/grpc/blob/master/doc/health-checking.md
   *
   * @example
   * grpc.check("hero_service", "hero.health.v1")
   *
   * @example
   * // Change the timeout
   * grpc.checkService('hero_service', 'hero.health.v1', { timeout: 300 })
   *
   * @param data The health indicators which should be checked
   */
  check(data: HealthCheckRequest): Observable<HealthCheckResponse>;

  watch(data: HealthCheckRequest): Observable<HealthCheckResponse>;
}
