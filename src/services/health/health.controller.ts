import {
  HealthCheckRequest,
  HealthCheckResponse,
  ServingStatus,
} from "./health";
import { GrpcMethod } from "@nestjs/microservices";
import { Sequelize } from "sequelize-typescript";
import { Controller } from "@nestjs/common";
import { from, Observable } from "rxjs";

/**
 * Handles Health Checks which can be used in
 * Controllers.
 */
@Controller()
export class HealthController {
  constructor(private readonly sequelize: Sequelize) {}

  @GrpcMethod("Health", "Check")
  check(data: HealthCheckRequest): Observable<HealthCheckResponse> {
    return from(this.testConnection(data));
  }

  @GrpcMethod("Health", "Watch")
  watch(data: HealthCheckRequest): Observable<HealthCheckResponse> {
    return from(this.testConnection(data));
  }

  private testConnection(data: HealthCheckRequest) {
    console.log(data);
    return this.sequelize
      .authenticate({
        logging: false,
        raw: false,
      })
      .then(() => {
        return {
          status: ServingStatus.SERVING,
        };
      })
      .catch(() => {
        return {
          status: ServingStatus.NOT_SERVING,
        };
      });
  }
}
