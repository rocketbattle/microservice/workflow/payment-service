/**
 * The status of the request service
 * @internal
 */
export enum ServingStatus {
  UNKNOWN = 0,
  SERVING = 1,
  NOT_SERVING = 2,
}

/**
 * The interface for the GRPC HealthService check request
 * @internal
 */
export interface HealthCheckRequest {
  service: string;
}

/**
 * The response of the health check
 * @internal
 */
export interface HealthCheckResponse {
  status: ServingStatus;
}
