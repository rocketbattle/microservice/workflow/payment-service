const { DATE, INTEGER, STRING, DECIMAL } = require("sequelize");

module.exports = {
  up: (query) => {
    return query.createTable("payments", {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      value: {
        type: DECIMAL,
        allowNull: false,
      },
      transactionId: {
        type: STRING(36),
        allowNull: false,
        field: "transaction_id",
      },
      phaseId: {
        type: STRING(10),
        allowNull: false,
        field: "phase_id",
        defaultValue: "PENDING",
      },
      createdAt: {
        type: DATE,
        allowNull: false,
        field: "created_at",
      },
    });
  },

  down: (query) => {
    return query.dropTable("payments");
  },
};
