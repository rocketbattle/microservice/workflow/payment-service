const { config } = require("dotenv");

config({
  path: process.env.NODE_ENV === "test" ? ".env.test" : ".env",
});

const configDatabase = {
  host: process.env.DATABASE_HOST,
  database: process.env.DATABASE_NAME,
  username: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASS,
  dialect: "postgres",
  timezone: process.env.DATABASE_TIMEZONE || "Etc/GMT+3",
  dialectOptions: {
    charset: "utf8mb4",
    timezone: process.env.DATABASE_TIMEZONE || "Etc/GMT+3",
  },
  define: {
    charset: "utf8mb4",
    timestamps: false,
  },
  pool: {
    max: parseInt(process.env.DATABASE_POOL_MAX) || 50,
    min: parseInt(process.env.DATABASE_POOL_MIN) || 1,
    acquire: parseInt(process.env.DATABASE_POOL_ACQUIRE) || 30000,
    idle: parseInt(process.env.DATABASE_POOL_IDLE) || 10000,
  },
};

module.exports = configDatabase;
