const { v4 } = require("uuid");

module.exports = {
  up: (query) => {
    return query.bulkInsert(
      "payments",
      [
        {
          id: 1,
          value: Math.random(),
          transaction_id: v4(),
          created_at: new Date(),
        },
      ],
      {},
    );
  },

  down: (query) => {
    return query.bulkDelete("payments", null, {});
  },
};
