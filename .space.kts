job("build") {
  container("node:lts-alpine") {
    resources {
      memory = 512
    }
    script { api ->
      api.run {
        "yarn install"
      }
      api.run {
        "yarn build"
      }
    }
  }
}
