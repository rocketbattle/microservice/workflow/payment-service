FROM node:lts-alpine

LABEL MAINTAINER="Ismael Queiroz <eu@queiroz.dev>"

# default to timeZone
ENV TZ America/Sao_Paulo

# default to UTF-8 file.encoding
ENV LANG C.UTF-8

RUN mkdir -p /home/node/app

# copy all files from current dir to /app in container
COPY --chown=node ./package-lock.json ./home/node/app/package-lock.json
COPY --chown=node ./package.json ./home/node/app/package.json
COPY --chown=node ./dist/ ./home/node/app/

# set /app directory as default working directory
WORKDIR /home/node/app

# install dependencies and health check, alter permission
RUN NPM_CONFIG_LOGLEVEL=error npm i --prod && \
  GRPC_HEALTH_PROBE_VERSION=v0.3.2 && \
    URL=https://github.com/grpc-ecosystem/grpc-health-probe/releases/download && \
    wget -qO /bin/grpc $URL/$GRPC_HEALTH_PROBE_VERSION/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc && \
    chown node:node -R /home/node

# set user
USER node

EXPOSE 5000

CMD [ "node", "main.js" ]
