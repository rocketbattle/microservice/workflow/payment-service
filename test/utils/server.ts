import { options } from "../../src/client.options";
import { AppModule } from "../../src/app.module";
import { Test } from "@nestjs/testing";

export const server = async () => {
  const moduleFixture = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();

  const app = moduleFixture.createNestApplication();
  app.connectMicroservice(options);

  return app;
};

export const app = server();
