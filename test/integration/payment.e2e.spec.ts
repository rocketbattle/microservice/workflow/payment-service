import { PaymentService } from "../../src/services/payment/payment.service";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { app } from "../utils/server";
import { join } from "path";

describe("PaymentService", () => {
  beforeAll(() => {
    return app
      .then((a) => a.startAllMicroservices())
      .catch((e) => console.error(e));
  });

  afterAll(async () => {
    return app.then((a) => a.close()).catch((e) => console.error(e));
  });

  it("/FindOne", async () => {
    const client = new ClientGrpcProxy({
      package: "payment.v1",
      url: "0.0.0.0:5000",
      protoPath: join(__dirname, "../../src/services/payment/payment.proto"),
    });

    const result = client.getService<PaymentService>("PaymentService").findOne({
      id: 1,
    });

    return expect(
      new Promise((resolve) => {
        result.subscribe((p) => {
          resolve(p.id);
        });
      }),
    ).resolves.toBe(1);
  });
});
