import { HealthService } from "../../src/services/health/health.service";
import { ServingStatus } from "../../src/services/health/health";
import { ClientGrpcProxy } from "@nestjs/microservices";
import { app } from "../utils/server";
import { map } from "rxjs/operators";
import { join } from "path";

const client = new ClientGrpcProxy({
  protoPath: join(__dirname, "../../src/services/health/health.proto"),
  package: "grpc.health.v1",
  url: "0.0.0.0:5000",
});

describe("HealthService", () => {
  beforeAll(() => {
    return app
      .then((a) => a.startAllMicroservices())
      .catch((e) => console.error(e));
  });

  afterAll(async () => {
    return app.then((a) => a.close()).catch((e) => console.error(e));
  });

  it("/Check", () => {
    const result = client.getService<HealthService>("Health").check({
      service: "postgres",
    });

    const status = result.pipe(
      map((r) => {
        return r.status;
      }),
    );

    return expect(
      new Promise((resolve) => {
        status.subscribe((s) => resolve(s));
      }),
    ).resolves.toBe(ServingStatus.SERVING);
  });

  it("/Watch", () => {
    const result = client.getService<HealthService>("Health").watch({
      service: "postgres",
    });

    const status = result.pipe(
      map((r) => {
        return r.status;
      }),
    );

    return expect(
      new Promise((resolve) => {
        status.subscribe((s) => resolve(s));
      }),
    ).resolves.toBe(ServingStatus.SERVING);
  });
});
